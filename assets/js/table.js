const dataHeader = [
    {
        key: 1,
        title: "Name",
        class: "sorting-asc"
    },
    {
        key: 2,
        title: "Position",
    },
    {
        key: 3,
        title: "Office",
    },
    {
        key: 4,
        title: "Age",
    },
    {
        key: 5,
        title: "Start date",
    }
]

var source = [
    ['Tiger Nixon', 'System Architect', 'Edinburgh', '5421', '2011/04/25', '$320,800'],
    ['Garrett Winters', 'Accountant', 'Tokyo', '8422', '2011/07/25', '$170,750'],
    ['Ashton Cox', 'Junior Technical Author', 'San Francisco', '1562', '2009/01/12', '$86,000'],
    ['Cedric Kelly', 'Senior Javascript Developer', 'Edinburgh', '6224', '2012/03/29', '$433,060'],
    ['Airi Satou', 'Accountant', 'Tokyo', '5407', '2008/11/28', '$162,700'],
    ['Brielle Williamson', 'Integration Specialist', 'New York', '4804', '2012/12/02', '$372,000'],
    ['Herrod Chandler', 'Sales Assistant', 'San Francisco', '9608', '2012/08/06', '$137,500'],
    ['Rhona Davidson', 'Integration Specialist', 'Tokyo', '6200', '2010/10/14', '$327,900'],
    ['Colleen Hurst', 'Javascript Developer', 'San Francisco', '2360', '2009/09/15', '$205,500'],
    ['Sonya Frost', 'Software Engineer', 'Edinburgh', '1667', '2008/12/13', '$103,600'],
    ['Jena Gaines', 'Office Manager', 'London', '3814', '2008/12/19', '$90,560'],
    ['Quinn Flynn', 'Support Lead', 'Edinburgh', '9497', '2013/03/03', '$342,000'],
    ['Charde Marshall', 'Regional Director', 'San Francisco', '6741', '2008/10/16', '$470,600'],
    ['Haley Kennedy', 'Senior Marketing Designer', 'London', '3597', '2012/12/18', '$313,500'],
    ['Tatyana Fitzpatrick', 'Regional Director', 'London', '1965', '2010/03/17', '$385,750'],
    ['Michael Silva', 'Marketing Designer', 'London', '1581', '2012/11/27', '$198,500'],
    ['Paul Byrd', 'Chief Financial Officer (CFO)', 'New York', '3059', '2010/06/09', '$725,000'],
    ['Gloria Little', 'Systems Administrator', 'New York', '1721', '2009/04/10', '$237,500'],
    ['Bradley Greer', 'Software Engineer', 'London', '2558', '2012/10/13', '$132,000'],
    ['Dai Rios', 'Personnel Lead', 'Edinburgh', '2290', '2012/09/26', '$217,500'],
    ['Jenette Caldwell', 'Development Lead', 'New York', '1937', '2011/09/03', '$345,000'],
    ['Yuri Berry', 'Chief Marketing Officer (CMO)', 'New York', '6154', '2009/06/25', '$675,000'],
    ['Caesar Vance', 'Pre-Sales Support', 'New York', '8330', '2011/12/12', '$106,450'],
    ['Doris Wilder', 'Sales Assistant', 'Sydney', '3023', '2010/09/20', '$85,600'],
    ['Angelica Ramos', 'Chief Executive Officer (CEO)', 'London', '5797', '2009/10/09', '$1,200,000'],
    ['Gavin Joyce', 'Developer', 'Edinburgh', '8822', '2010/12/22', '$92,575'],
    ['Jennifer Chang', 'Regional Director', 'Singapore', '9239', '2010/11/14', '$357,650'],
    ['Brenden Wagner', 'Software Engineer', 'San Francisco', '1314', '2011/06/07', '$206,850'],
    ['Fiona Green', 'Chief Operating Officer (COO)', 'San Francisco', '2947', '2010/03/11', '$850,000'],
    ['Shou Itou', 'Regional Marketing', 'Tokyo', '8899', '2011/08/14', '$163,000'],
    ['Michelle House', 'Integration Specialist', 'Sydney', '2769', '2011/06/02', '$95,400'],
    ['Suki Burks', 'Developer', 'London', '6832', '2009/10/22', '$114,500'],
    ['Prescott Bartlett', 'Technical Author', 'London', '3606', '2011/05/07', '$145,000'],
    ['Gavin Cortez', 'Team Leader', 'San Francisco', '2860', '2008/10/26', '$235,500'],
    ['Martena Mccray', 'Post-Sales support', 'Edinburgh', '8240', '2011/03/09', '$324,050'],
    ['Unity Butler', 'Marketing Designer', 'San Francisco', '5384', '2009/12/09', '$85,675'],
];

var dataSet = [
    ['Tiger Nixon', 'System Architect', 'Edinburgh', '5421', '2011/04/25', '$320,800'],
    ['Garrett Winters', 'Accountant', 'Tokyo', '8422', '2011/07/25', '$170,750'],
    ['Ashton Cox', 'Junior Technical Author', 'San Francisco', '1562', '2009/01/12', '$86,000'],
    ['Cedric Kelly', 'Senior Javascript Developer', 'Edinburgh', '6224', '2012/03/29', '$433,060'],
    ['Airi Satou', 'Accountant', 'Tokyo', '5407', '2008/11/28', '$162,700'],
    ['Brielle Williamson', 'Integration Specialist', 'New York', '4804', '2012/12/02', '$372,000'],
    ['Herrod Chandler', 'Sales Assistant', 'San Francisco', '9608', '2012/08/06', '$137,500'],
    ['Rhona Davidson', 'Integration Specialist', 'Tokyo', '6200', '2010/10/14', '$327,900'],
    ['Colleen Hurst', 'Javascript Developer', 'San Francisco', '2360', '2009/09/15', '$205,500'],
    ['Sonya Frost', 'Software Engineer', 'Edinburgh', '1667', '2008/12/13', '$103,600'],
    ['Jena Gaines', 'Office Manager', 'London', '3814', '2008/12/19', '$90,560'],
    ['Quinn Flynn', 'Support Lead', 'Edinburgh', '9497', '2013/03/03', '$342,000'],
    ['Charde Marshall', 'Regional Director', 'San Francisco', '6741', '2008/10/16', '$470,600'],
    ['Haley Kennedy', 'Senior Marketing Designer', 'London', '3597', '2012/12/18', '$313,500'],
    ['Tatyana Fitzpatrick', 'Regional Director', 'London', '1965', '2010/03/17', '$385,750'],
    ['Michael Silva', 'Marketing Designer', 'London', '1581', '2012/11/27', '$198,500'],
    ['Paul Byrd', 'Chief Financial Officer (CFO)', 'New York', '3059', '2010/06/09', '$725,000'],
    ['Gloria Little', 'Systems Administrator', 'New York', '1721', '2009/04/10', '$237,500'],
    ['Bradley Greer', 'Software Engineer', 'London', '2558', '2012/10/13', '$132,000'],
    ['Dai Rios', 'Personnel Lead', 'Edinburgh', '2290', '2012/09/26', '$217,500'],
    ['Jenette Caldwell', 'Development Lead', 'New York', '1937', '2011/09/03', '$345,000'],
    ['Yuri Berry', 'Chief Marketing Officer (CMO)', 'New York', '6154', '2009/06/25', '$675,000'],
    ['Caesar Vance', 'Pre-Sales Support', 'New York', '8330', '2011/12/12', '$106,450'],
    ['Doris Wilder', 'Sales Assistant', 'Sydney', '3023', '2010/09/20', '$85,600'],
    ['Angelica Ramos', 'Chief Executive Officer (CEO)', 'London', '5797', '2009/10/09', '$1,200,000'],
    ['Gavin Joyce', 'Developer', 'Edinburgh', '8822', '2010/12/22', '$92,575'],
    ['Jennifer Chang', 'Regional Director', 'Singapore', '9239', '2010/11/14', '$357,650'],
    ['Brenden Wagner', 'Software Engineer', 'San Francisco', '1314', '2011/06/07', '$206,850'],
    ['Fiona Green', 'Chief Operating Officer (COO)', 'San Francisco', '2947', '2010/03/11', '$850,000'],
    ['Shou Itou', 'Regional Marketing', 'Tokyo', '8899', '2011/08/14', '$163,000'],
    ['Michelle House', 'Integration Specialist', 'Sydney', '2769', '2011/06/02', '$95,400'],
    ['Suki Burks', 'Developer', 'London', '6832', '2009/10/22', '$114,500'],
    ['Prescott Bartlett', 'Technical Author', 'London', '3606', '2011/05/07', '$145,000'],
    ['Gavin Cortez', 'Team Leader', 'San Francisco', '2860', '2008/10/26', '$235,500'],
    ['Martena Mccray', 'Post-Sales support', 'Edinburgh', '8240', '2011/03/09', '$324,050'],
    ['Unity Butler', 'Marketing Designer', 'San Francisco', '5384', '2009/12/09', '$85,675'],
];



var tableElement = document.getElementById("table")
var dataLength = document.getElementById("data-length")
var rows = document.getElementsByClassName("row")
var elPageNumber = document.getElementById("pagination__page-number")
var tableHead = document.getElementById("table-head")
var tableBody = document.getElementById("table-body")
var btnPre = document.getElementById("btn-pre")
var btnNext = document.getElementById("btn-next")
var search = document.getElementById("search-input")
var info = document.getElementById("info")


var header = "";
var body = "";

dataHeader.map(element => {
    header += `<th class="sorting ${element.class ? element.class : ""}">${element.title}</th>`
})
header = `<tr>${header}</tr>`
tableHead.innerHTML = header



//show salary
// for (let i = 0; i < rows.length; i++) {
//     let firstCell = rows[i].firstChild;
//     firstCell.classList.add("first-cell")
//     firstCell.addEventListener("click", () => {
//         let table = document.getElementById("table")
//         if ([...firstCell.classList].includes("have-child")) {
//             firstCell.classList.remove("have-child")
//         }
//         else {
//             firstCell.classList.add("have-child")
//             let rowInsert = table.insertRow(i + 2)
//             rowInsert.classList.add("child")
//             let cellInsert = rowInsert.insertCell(0)
//             cellInsert.id = "child"
//             document.getElementById("child").colSpan = "4"
//             cellInsert.innerHTML = "<span class=font-strong>Salary </span>" + `${rows[i].lastChild.textContent}`
//         }

//     })
// }

function updateInfo(start, end, total) {
    let text = `Showing ${start + 1} to ${end} of ${total} entries`
    info.innerText = text
}

function loadDataTable(limit, page) {
    let dataShow = []
    let start = limit * (page - 1);
    let end = (limit * page < dataSet.length) ? limit * page : dataSet.length
    for (let i = start; i < end; i++) {
        dataShow.push(dataSet[i])
    }
    let bodyTable = "";
    dataShow.map((dataRow) => {
        let row = "";
        dataRow.map(dataCell => {
            row += `<td>${dataCell}</td>`
        })
        bodyTable += `<tr class="row">${row}</tr>`
    })
    tableBody.innerHTML = bodyTable
    updateInfo(start, end, dataSet.length)
}

function setPagination(limit) {
    let pages = Math.floor(dataSet.length / limit)
    pages = (dataSet.length % limit === 0) ? pages : pages + 1
    let str = "";
    for (let i = 0; i < pages; i++)
        str += `<button onclick="changePage(${i + 1})" class="btn-pagination btn-hover btn-number">${i + 1}</button>`
    elPageNumber.innerHTML = str
    elPageNumber.firstChild.classList.add("selected")
}

window.onload = function () {
    let limit = Number(dataLength.value)
    loadDataTable(limit, 1)
    //sort data
    dataSet.sort((a, b) => {
        if (a[0] < b[0])
            return -1
        else
            if (a[0 > b[0]])
                return 1
        return 0
    })

    // pagination
    setPagination(limit)
    changePage(1)
}

dataLength.addEventListener("change", (e) => {
    e.preventDefault()
    let limit = Number(e.target.value)
    loadDataTable(limit, 1)
    setPagination(limit)
    changePage(1)
})

//change page
function changePage(page) {
    let limit = Number(dataLength.value)
    let listNode = [...elPageNumber.childNodes]

    //caculate total page
    let pages = Math.floor(dataSet.length / limit)
    pages = (dataSet.length % limit === 0) ? pages : pages + 1
    listNode.map(el => {
        el.classList.remove("selected")
    })
    loadDataTable(limit, Number(page))
    listNode[page - 1].classList.add("selected")
    if (page > 1) {
        btnPre.classList.add("btn-hover")
        btnPre.disabled = false;
    }
    else
        if (page === 1) {
            btnPre.classList.remove("btn-hover")
            btnPre.disabled = true;
        }
    if (page === pages) {
        btnNext.disabled = true;
        btnNext.classList.remove("btn-hover")
    }
    else
        if (page < pages) {
            btnNext.disabled = false;
            btnNext.classList.add("btn-hover")
        }

}
//btn-pre
function btnNextPre(num) {
    let listNode = [...elPageNumber.childNodes]
    let currentPage = 1
    listNode.map(element => {
        let arr = [...element.classList]
        if (arr.includes("selected"))
            currentPage = Number(element.textContent)
    })
    changePage(currentPage + num)
}



// search
search.addEventListener("input", (e) => {
    e.preventDefault();
    let limit = Number(dataLength.value)
    let filter = e.target.value
    let newArr = source.filter(element => {
        let bool = false
        element.map(item => {
            if (item.toLowerCase().includes(filter)) {
                bool = true
            }
        })
        return bool
    })
    dataSet = [...newArr]
    loadDataTable(limit, 1)
    setPagination(limit)
})


//sort
function sortAtoZ(num) {
    //sort data a-z
    return dataSet.sort((a, b) => {
        if (a[num] < b[num])
            return -1
        else
            if (a[num] > b[num])
                return 1
        return 0
    })
}

function sortZtoA(num) {
    dataSet.sort((a, b) => {
        if (a[num] > b[num].charAt(0))
            return -1
        else
            if (a[num] < b[num])
                return 1
        return 0
    })
}

var sorting = document.getElementsByClassName("sorting")
let listSorting = [...sorting]
for (let i = 0; i < listSorting.length; i++)
    listSorting[i].addEventListener("click", () => {
        let limit = Number(dataLength.value)
        let listClass = [...listSorting[i].classList]
        if (listClass.includes("sorting-desc")) {
            listSorting[i].classList.remove("sorting-desc")
            listSorting[i].classList.add("sorting-asc")
            //sort data a-z
            sortAtoZ(i)
            loadDataTable(limit, 1)
            setPagination(limit)
        }
        else
            if (listClass.includes("sorting-asc")) {
                listSorting[i].classList.remove("sorting-asc")
                listSorting[i].classList.add("sorting-desc")
                //sort data z-a
                sortZtoA(i)
                loadDataTable(limit, 1)
                setPagination(limit)
            }
            else
                if (!listClass.includes("sorting-desc") && !listClass.includes("sorting-asc")) {
                    for (let j = 0; j < listSorting.length; j++) {
                        listSorting[j].classList.remove("sorting-desc")
                        listSorting[j].classList.remove("sorting-asc")
                    }
                    listSorting[i].classList.add("sorting-asc")
                    //sort a-z
                    sortAtoZ(i)
                    loadDataTable(limit, 1)
                    setPagination(limit)
                }
    })











